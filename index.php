<?php

$yii = '/var/framework/yii/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
ini_set('display_errors', true);
error_reporting(E_ALL);
require_once($yii);
Yii::createWebApplication($config)->run();
?>